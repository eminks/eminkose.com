---
title: '{{ replace .File.ContentBaseName "-" " " | title }}'
# weight: 1
# aliases: ["/first"]
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
disableHLJS: false # to disable highlightjs
disableShare: true
disableHLJS: false
hideSummary: true
searchHidden: true
ShowReadingTime: false
ShowBreadCrumbs: false
ShowPostNavLinks: false
ShowWordCount: false
ShowRssButtonInSectionTermList: false
UseHugoToc: false
cover:
    image: "<image path/url>" # image path/url
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---

---
title: "Telegram Bot APİ kullanımı"
url: telegram-bot-api-kullanimi
weight: 5
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: true
searchHidden: true
ShowReadingTime: false
ShowBreadCrumbs: false
ShowPostNavLinks: false
ShowWordCount: false
ShowRssButtonInSectionTermList: false
UseHugoToc: false
cover:
    image: "telegram-bots.jpg" # image path/url
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---

# Telegram Bot API Kullanımı

Anlık mesajlaşma platformlarından biri olan Telegram bir kaç ay önce bot için API tanıttı. Bu API ile birlikte oldukça kolay bir biçimde bot yazabiliyoruz. Bende bir süredir bu konuda çalışmak istiyordum fakat okulum dolayısıyla zaman bulamıyordum. Okulun bitmesini fırsat bilip kolları sıvadım. Küçük bir araştırma sonrası yapmayı başardım. Bundan sonra yazabileceğiniz botlar sizin bilgi ve hayal gücünüze kalmış :)

## Örnek Botlar

* **Movies Tracker Bot**: İmdb üzerinden film aramanıza imkan tanıyan bir bot
* **Get Music Bot**: Youtube ve SoundCloud üzerinden mp3 indirmenize olanak tanıyan bir bot
* **Mafia Gangs Game**: Mafya gangsterini canlandırdığınız bir oyun puan kazanmaya çalışıyorsunuz

*Daha fazla bot örneği için: storebot.me*

## Telegram Bot Yazımı

Daha anlaşılır olması için iki bölüm halinde anlatmaya karar verdim.

### Bölüm 1: Bot Oluşturma

1. Telegramın sunduğu **BotFather** botunu kullanmamız gerek.
2. Arama kısmına **@BotFather** yazarak veya yazının üstüne tıklayarak bota erişin.
3. İlk olarak `/newbot` diyerek botumuzu oluşturmaya başlıyoruz ve bizden botumuz için bir ad vermemizi istiyor ben "Küçük Tosun" olarak belirledim.
4. Şimdi bizden bir kullanıcı adı girmemizi istiyor (Bu kısımda boşluk bırakamıyorsunuz). Ben kullanıcı adını "tosunbot" olarak belirliyorum.
5. Bu adımdan sonra size gereken API anahtarını veriyor. Örn: `222875154:AaGkHc4efih5jxwgQ7IBcGw1r57HNOPEnm0`

Bu kısımdan sonra bota erişmek için kod yazmaya başlayabilirsiniz. Ben botun çeşitli özelliklerini değiştirerek daha güzel bir hale getirmek istiyorum. İsteyenle direk Bölüm 2'ye geçebilir.

#### Kullanılabilir Komutlar

Yazı bölümünde sağda bulunan gülücük simgesinin yanındaki kutuya tıklayarak kullanabileceğimiz çeşitli komutları görebiliriz:

* `/setdescription`: Botumuza açıklama eklemek için kullanıyoruz
* `/setuserpic`: Botumuza profil resmi tanımlamak için kullanıyoruz
* `/setabouttext`: Botumuza küçük bir hakkımda yazısı tanımlamak için kullanıyoruz

### Bölüm 2: Kod Yazımı

Bu bölümde PHP ile gerekli kodları yazacağız:

```php
<?php
$token = "222875154:AaGkHc4efih5jxwgQ7IBcGw1r57HNOPEnm0"; //Tokenimiz
$api = "https://api.telegram.org/bot".$token; //api bağlantısı

$up = file_get_contents("php://input"); //Çıktıyı alıyoruz

$upary = json_decode($up, TRUE); //Json formatındaki veriyi deşifre edip arrya çeviriyoruz

$cid = $upary["message"]["chat"]["id"]; //Chat idsini alıyoruz

$mesaj = $upary["message"]["text"]; //Mesajı alıyoruz

switch ($mesaj) {
case stristr($string, "merhaba");:
    file_get_contents($api."/sendMessage?chat_id=".$cid."&text=Merhaba"); //Kullanıcıya "Merhaba" mesajını atıyoruz
    break;
case stristr($string, "Nasılsın");:
    file_get_contents($api."/sendMessage?chat_id=".$cid."&text=İyidir sen?");
    break;
default:
    file_get_contents($api."/sendMessage?chat_id=".$cid."&text=Yazdığın şeyi anlayamadım?");
}
?>
```

## Kurulum Tamamlama

Botumuzun kurulumunu tamamlamak için aşağıdaki adrese giderek Webhook özelliğini aktif ediyoruz:

```
https://api.telegram.org/bot222875154:AaGkHc4efih5jxwgQ7IBcGw1r57HNOPEnm0/setwebhook?https://domain/telbot.php
```

### Not

Webhook özelliğinin çalışması için sitenizin SSL sertifikası bulunması gerek. Bunu ücretsiz olarak cloudflare üzerinden alabilirsiniz.
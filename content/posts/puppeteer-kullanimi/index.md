---
title: 'Puppeteer kullanımı'
weight: 2
url: puppeteer-kullanimi
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
disableHLJS: false # to disable highlightjs
disableShare: true
disableHLJS: false
hideSummary: true
searchHidden: true
ShowReadingTime: false
ShowBreadCrumbs: false
ShowPostNavLinks: false
ShowWordCount: false
ShowRssButtonInSectionTermList: false
UseHugoToc: false
cover:
    image: "pup_arka.jpg" # image path/url
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---
# Puppeteer ile Web Scraping

## Giriş

Ben aslında bir back-end developer'ım fakat JavaScript oldukça büyük bir ivme kazandı diyebilirim. Bu nedenle ister istemez bazı yenilikleri öğrenmek gerekiyor. Puppeteer aslında bir tür arayüzü olmayan tarayıcı diyebilirim. Bu yüzden headless Chrome olarak da geçiyor. Bir çok özelliği var ve tek yazıda anlatmak pek mümkün değil. Bu yazıda basit bir örneği yapacağız diyebilirim. Herhangi web sitesine girip sitenin ekran görüntüsünü, başlığını, açıklamasını alacağız.

## Ön Gereksinimler

- İyi düzeyde JavaScript bilgisi
- Node.js kurulumu (Nasıl kurulacağını arama motorlarında bulabilirsiniz)

## Kurulum

Öncelikle sistemimize Puppeteer'ı kuruyoruz:

```bash
npm install --save puppeteer
```

Not: Kurulum biraz uzun sürüyor çünkü kurarken aynı zamanda Chromium'u da indiriyorsunuz.

## Temel Kullanım

1. Puppeteer'ı projeye dahil etme:
```javascript
const puppeteer = require('puppeteer');
```

2. Tarayıcıyı başlatma:
```javascript
const tarayici = await puppeteer.launch();
```

3. Yeni sekme açma:
```javascript
const sekme = await tarayici.newPage();
```

4. Web sitesine gitme:
```javascript
await sekme.goto("https://eminkose.com");
```

5. Ekran görüntüsü alma:
```javascript
await sekme.screenshot({path: 'demo.png', fullPage: true});
```

6. Sayfa başlığını alma:
```javascript
const title = await sekme.title();
```

7. Sayfa açıklamasını alma:
```javascript
const description = await sekme.evaluate(() => 
    document.querySelector("meta[name='description']").getAttribute("content")
);
```

8. Verileri JSON formatında toplama:
```javascript
var json = [ "demo.png", title, description];
```

9. Sonuçları yazdırma ve tarayıcıyı kapatma:
```javascript
console.log(json);
await tarayici.close();
```

## Farklı Cihaz Emülasyonu

Farklı cihazlarda test yapmak için:

1. Cihaz tanımlamalarını içe aktarma:
```javascript
const devices = require('puppeteer/DeviceDescriptors');
```

2. Cihaz emülasyonunu başlatma:
```javascript
await sekme.emulate(devices['Nexus 6']);
```

## Faydalı Linkler

- [Cihaz Listesi](https://github.com/GoogleChrome/puppeteer/blob/master/DeviceDescriptors.js)
- [Proje Kaynak Kodu](https://gitlab.com/eminks/puppeteerproje)
- [Resmi Dökümantasyon](https://github.com/GoogleChrome/puppeteer/blob/v1.8.0/docs/api.md)

## Kaynaklar

- [Puppeteer GitHub](https://github.com/GoogleChrome/puppeteer)
- [Puppeteer API Dökümantasyonu](https://github.com/GoogleChrome/puppeteer/blob/v1.8.0/docs/api.md)
- [Video Tutorial](https://www.youtube.com/watch?v=pixfH6yyqZk)
---
title: "Deno - Nodejs'nin alternatifi"
weight: 1
url: deno-nodejsnin-alternatifi
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
disableHLJS: false # to disable highlightjs
disableShare: true
disableHLJS: false
hideSummary: true
searchHidden: true
ShowReadingTime: false
ShowBreadCrumbs: false
ShowPostNavLinks: false
ShowWordCount: false
ShowRssButtonInSectionTermList: false
UseHugoToc: false
cover:
    image: "deno_arka.jpg" # image path/url
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---

Geçtiğimiz günlerde takip ettiğim youtube kanallarından [birinde](https://www.youtube.com/watch?v=F0G9lZ7gecE) "deno" isimli javascript runtime (javascrip derleyisi)'ı ile tanıştım. Kendisini sitesinde "A secure runtime for JavaScript and TypeScript" olarak tanıyor. Bu yazıda çok detayına girmeden sadece benim ilgimi çeken kısımları sizin ile paylaşmak istiyorum.

## Neden deno?

### Oldukça güvenli

Sizin için ne kadar önemli bilemiyorum fakat güvenlik benim ve deno için önemli ölçülerden birisi diyebilirim. Bu yüzden siz özel olarak izin vermedikçe yazdığınız kodlar;

*   Dosya sistemi
*   Ağ
*   Diğer dosyaları çalıştırma
*   Environment variables'lara

erişemiyor, sadece bir sandbox içerisinde çalıştırılıyor. Bu size büyük bir güvenlik avantajı sağlıyor.

### Npm, yarn vs. bağımlılığından kurtulun

Aklınıza nodejs geldiğinde muhtemelen **npm vs yarn** kavgası da gelecektir. Eğer deno kullanıyorsanız bu kavganın bir tarafı olmak zorunda değilsiniz, daha iyisi bu tür servislere bağımlı olmak zorunda da değilsiniz.

> import { assertEquals } from "https://deno.land/std/testing/asserts.ts";  

Yukarda görüldüğü gibi çok basit bir şekilde istediğiniz paketi dosyanıza ekleyebilirsiniz. Özellikle paket geliştirenlerin bunu çok seveceğini düşünüyorum. Eğer ingilizceniz varsa [şu videoyu](https://www.youtube.com/watch?v=MO8hZlgK5zc "The economics of open source by C J Silverio | JSConf EU 2019") da izlemenizi tavsiye ederim. 

## Yeni!

Son bir kaç yıldır yazılımcıların en büyük şikayetlerinden birisi çok fazla öğrenilecek şey olması. Deno bu soruna katkı sağlıyor olsa da ilgilenmenize değer bir proje diyebilirim. 

### _So Long, and Thanks for All the Fish_

Bu yazıda çok basit bir giriş yapmak istedim. Daha detaylı birşeyler arıyorsanız kaynaklar kısmında bazı linkler bırakıyorum. Son olarak deno'yu duyduğum anda aklıma gelen ve çocukluğumun en havalı figürü olan **Denver**'ı anmadan geçmek olmaz! :)

{{< youtube D5tXJ0nq0vc >}}

### Kaynaklar ve yararlı linkler:

[https://www.youtube.com/watch?v=F0G9lZ7gecE](https://www.youtube.com/watch?v=F0G9lZ7gecE "Deno in 100 Seconds")  

[https://www.youtube.com/watch?v=MO8hZlgK5zc](https://www.youtube.com/watch?v=MO8hZlgK5zc "The economics of open source by C J Silverio | JSConf EU 2019")

[https://www.youtube.com/watch?v=1gIiZfSbEAE](https://www.youtube.com/watch?v=1gIiZfSbEAE "Deno is a New Way to JavaScript - Ryan Dahl & Kitson Kelly")  

[https://blog.logrocket.com/what-is-deno/](https://blog.logrocket.com/what-is-deno/ "What’s Deno, and how is it different from Node.js?")  

[https://dev.to/gregfletcher/what-is-deno-js-and-why-should-you-care-b26](https://dev.to/gregfletcher/what-is-deno-js-and-why-should-you-care-b26 " What is Deno.js and why should you care? ")
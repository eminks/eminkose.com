---
title: 'Imgur Api'
weight: 3
url: imgur-api
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
disableHLJS: false # to disable highlightjs
disableShare: true
disableHLJS: false
hideSummary: true
searchHidden: true
ShowReadingTime: false
ShowBreadCrumbs: false
ShowPostNavLinks: false
ShowWordCount: false
ShowRssButtonInSectionTermList: false
UseHugoToc: false
cover:
    image: "1_OdnnbyySEFlUXn9PTJTzhQ.png" # image path/url
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---
# Imgur API ile Resim Yükleme

## Giriş

Benim gibi internet ile uğraşan kişilerin en büyük sorunlarından birisi resimlerdir. Blogger gibi servisleri kullandığınızda bu sorun olmasa da kendi sunucunuz da site barındırdığınız da alan ve trafik konusunda sıkıntı çıkarıyor.

Bu nedenle Imgur gibi büyük bir siteye yüklemek daha mantıklıdır. Resimleriniz hem silinmez hem de alan ve trafik konusunda rahatlarsınız. Ben de yapmayı düşündüğüm bir script için bu konuyu araştırdım ve Imgur'un upload API'sini araştırdım. Biraz araştırmadan sonra bu konuda bir kullanım buldum ve biraz kendime göre düzenledim sizinle de paylaşmak istedim.

## Kurulum

Kullanmak isterseniz [bu adresten](https://api.imgur.com/oauth2/addclient) kendi uygulamanızı oluşturun sonrasında "client id" kısmını alıp aşağıdaki bölüme yazın. Gerisi sizin bilgi ve hayal gücünüze bağlı :).

## Kod

```php
<form method="post" enctype="multipart/form-data">
<input type="file" name="upload" /><br />
<br />
<input type="submit" value="Yükle" /><br />
</form>
</html>
<?php
error_reporting(0);

$client_id = '7bf37e0180cb90a';

$filetype = explode('/',mime_content_type($_FILES['upload']['tmp_name']));

$image = file_get_contents($_FILES['upload']['tmp_name']);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://api.imgur.com/3/image.json');
curl_setopt($ch, CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Authorization: Client-ID ' . $client_id ));
curl_setopt($ch, CURLOPT_POSTFIELDS, array( 'image' => base64_encode($image) ));

$reply = curl_exec($ch);

curl_close($ch);

$reply = json_decode($reply);

printf('<img height="180" src="%s" >', @$reply->data->link);

printf('%s', @$reply->data->link);
?>
```

## İndirme Linkleri

Script'i aşağıdaki linklerden indirebilirsiniz:

- [Cloud Mail](https://cloud.mail.ru/public/L37i/YiZEBQhTL)
- [Dosya.co](http://dosya.co/fpyipm5n0cjz/imgur.zip.html)
- [DFiles](http://dfiles.eu/files/t205ikngi)
- [Free.fr](http://dl.free.fr/getfile.pl?file=/zznAWkp9)
- [TusFiles](http://tusfiles.net/heyokt5zoiu1)
- [Share-Online](http://www.share-online.biz/dl/HS6CJ3TNMGA)
- [MegaCloudFiles](http://megacloudfiles.com/u260sci17r3f)
- [OpenLoad](https://openload.co/f/R2YJi2bzPGM/imgur.zip)
- [SolidFiles](http://www.solidfiles.com/d/42da21bff4/)
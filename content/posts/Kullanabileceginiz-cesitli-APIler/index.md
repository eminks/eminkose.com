---
title: 'Kullanabileceğiniz çeşitli APİler'
weight: 4
url: Kullanabileceginiz-cesitli-APIler
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
disableHLJS: false # to disable highlightjs
disableShare: true
disableHLJS: false
hideSummary: true
searchHidden: true
ShowReadingTime: false
ShowBreadCrumbs: false
ShowPostNavLinks: false
ShowWordCount: false
ShowRssButtonInSectionTermList: false
UseHugoToc: false
cover:
    image: "1_OdnnbyySEFlUXn9PTJTzhQ.png" # image path/url
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---
Çeşitli projelerimiz için bazen bilgi girmek ile uğraşmak yerine API kullanmak daha kolayımıza geliyor. Bende kullanabileceğiniz çeşitli APIleri derlemek istedim.

### İçerik ve Medya APIleri

#### [KAT.CR](https://kat.cr/api/)
Dünyaca ünlü bir torrent sitesi

#### [YTS](https://yts.ag/api)
Yüksek kalitedeki filmleri düşük boyutlu olarak paylaşan bir site

#### [OMDBAPI](http://www.omdbapi.com/)
IMDB için 3. parti olarak tasarlanmış bir API

#### [UNSPLASH.IT](https://unsplash.it/)
Çeşitli boyutlarda deneme için kullanılabilecek resimler sağlayan bir site

#### [CODEPEN](https://blog.codepen.io/documentation/)
Kod denemelerini paylaşmak için kullanılan bir site

#### [ICONFINDER](https://www.iconfinder.com/api-solution)
Binlerce icon barındıran bir site

#### [PIXABAY](https://pixabay.com/api/docs/)
Ücretsiz olarak çeşitli resim, video gibi içerikler sunan bir site

### Hava Durumu API

#### [OPENWEATHERMAP](http://openweathermap.org/api)
Detaylı hava durumu bilgileri sunan bir site

### Oyun APIleri

#### [RIOTGAMES](https://developer.riotgames.com/)
Ülkemizde LOL (League of Legends) oyunu ile tanınan Riotgames'in sunduğu servis

#### [BATTLE.NET](https://dev.battle.net/)
Ülkemizde çeşitli oyunlar ile tanınan Blizzard'ın sunduğu servis